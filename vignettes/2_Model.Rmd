---
title: "Model Bàsic"
author: "Andrés López-Sepulcre"
date: "8/20/2020"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

## Definició del Model

Defineixo aquí l'estructura bàsica del model d'estima poblacional bassat en dos mètodes de detecció diferents (visual i auditiu). El model bàsic conté un paràmetre biològic d'interés, la mida poblacional $N$, i tres paràmetres observacionals: la probabilitat d'escoltar un individu donat que hi és present. $p_a$,  la probabilitat d'observar un individu donat que s'ha sentit $p_{v|a}$, i la probabilitat d'observar un individu donat que hi és present i no ha estat sentit $p_{v|^\neg a}$. Aquestes definicions impliquen que suposem que, mentre que la detecció auditiva es independent de la visual, la visual no ho és de l'auditiva, Es a dir, escoltar un individu pot influenciar la probabilitat de veure-ho en dirigir l'atenció d l'observador cap a el só; però la probabilitat d'escoltar un individu no depen de si s'ha vist o no. Podem dir donçs que la probabilitat conjunta de veure un individu (sentit o no) es:

$$
p_v = p_{(v|^\neg a)} + p_{(v|a)}
$$

Donat això, podem definir la probabilitat de cada tipus d'observació, donada la presència d'un individu, que denotarem com a $\psi$:

$$
P(A) = p_a \cdot \psi \\
P(V) = p_v \cdot p_{(v|^\neg a)} \cdot \psi \\
P(AV) = p_a \cdot p_v \cdot \psi \\
P(VA) = p_v \cdot p_{(v|a)} \cdot \psi
$$


