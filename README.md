# Censos d'aus al Besós

Aquest paquet està dedicat a l'anàlisi de les dades de cens d'aus preses al 2019 i 2020 per en Xavi Larruy i en Xesco Macia. Les dades es van prendre en transectes, prenent dades de totes les espècies detectades en bandes de distància. Així mateix, es va diferenciar entre deteccions visuals, auditives, i tant visuals com auditives, per millorar l'estima de detectabilitat.


## Utilització del Paquet

Si ets membre del prijecte però només vols veure les anàlisis o informes dels resultats, els podras trobar sota la pestanya 'Articles' d'aquesta pàgina.

Si vols fer servir el paquet o has de contribuir-hi dades, documentació, o codi, deus clonar aquest paquet i instal.lar-lo al teu ordinador

Aquest projecte està versionat via `git` i ubicat a [GitLab](https://gitlab.com/) per permetre compartir i sincronitzar entre col.laboradors.

Per tenir una copia d'aquest paquet al teu ordinador deus:

- Tenir drets d'accès al repositori del projecte a GitLab a https://gitlab.com/lopezsepulcre/IPAbesos

- Tenir `git` instal.lat i funcionant al teu ordinador. Si no, ves a  https://git-scm.com per descarregar-lo.

- Si no has fet servit mai git de manera col.laborativa, probablement hauràs de crrar una clau SSH com descriuen [aquí](http://happygitwithr.com/ssh-keys.html).


Per clonar el projecte sencer, ves a la Terminal de comands i escriu:

```{bash, echo = TRUE, eval = FALSE}
git clone git@gitlab.com:lopezsepulcre/plantaginisCMR.git
```
 
Per contribuir, pots obrir el paquet amb RStudio fent 'click' a `IPAbesos.Rproj` file. Recorda fer "branch", "commit" i "request merges" amb el teu codi.

Per fer servir el paquet, simplement escriu a la consola de RStudio:

```{r, echo = TRUE, eval = FALSE}
library(IPAbesos)
```
